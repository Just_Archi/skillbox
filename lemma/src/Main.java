import org.apache.lucene.morphology.LuceneMorphology;
import org.apache.lucene.morphology.english.EnglishLuceneMorphology;
import org.apache.lucene.morphology.russian.RussianLuceneMorphology;


import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main
{
    private static final LuceneMorphology engMorph;
    private static final LuceneMorphology rusMorph;

    static
    {
        try
        {
            engMorph = new EnglishLuceneMorphology();
            rusMorph = new RussianLuceneMorphology();
        } catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }

    public static void main(String[] args) throws IOException
    {
        Map<String, Integer> lemmas = countLemmas("");
        for (Map.Entry<String, Integer> entry : lemmas.entrySet())
        {
            System.out.println(entry.getKey() + " - " + entry.getValue());
        }

    }

    public static Map<String, Integer> countLemmas(String str)
    {
        Map<String, Integer> lemmas = new HashMap<>();
        str = str.toLowerCase().replaceAll("[^�-�a-z\\s]", "");
        System.out.println(str);
        String[] words = str.split("\\s+");
        for (String word : words)
        {
            List<String> adds = defineLemmas(word);
            for (String add : adds)
            {
                if (lemmas.containsKey(add))
                {
                    int count = lemmas.get(add) + 1;
                    lemmas.put(add, count);
                }
                else
                {
                    lemmas.put(add, 1);
                }
            }
        }
        return lemmas;
    }

    public static List<String> defineLemmas(String word)
    {
        List<String> lemmas = new ArrayList<>();
        if (word.matches("[a-z]+"))
        {
            System.out.println("eng");
            if (word.length() != 0 && engCheckType(word))
            {
                List<String> wordBaseForms = engMorph.getMorphInfo(word);
                wordBaseForms.forEach(form -> lemmas.add(form.substring(0, form.indexOf("\s") - 2)));
            }
        }
        else
        {
            System.out.println("rus");
            if (word.length() != 0 && rusCheckType(word))
            {
                List<String> wordBaseForms = rusMorph.getMorphInfo(word);
                wordBaseForms.forEach(form -> lemmas.add(form.substring(0, form.indexOf("\s") - 2)));
            }
        }
        return lemmas;
    }

    public static boolean rusCheckType(String form)
    {
        String[] types = {"����", "�����", "����", "����"};
        String type = form.substring(form.lastIndexOf("\s") + 1);
        for (String currentType : types)
        {
            if (type.equals(currentType)) {return false;}
        }
        return true;
    }

    public static boolean engCheckType(String form)
    {
        String[] types = {"CONJ", "PREP", "PART", "INT"};
        String type = form.substring(form.lastIndexOf("\s") + 1);
        for (String currentType : types)
        {
            if (type.equals(currentType)) {return false;}
        }
        return true;
    }
}
