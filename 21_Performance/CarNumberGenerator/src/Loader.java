import java.io.FileOutputStream;

public class Loader
{

    public static void main(String[] args) throws Exception
    {
        long start = System.currentTimeMillis();

        FileOutputStream writer = new FileOutputStream("res/numbers.txt");
        FileOutputStream writer2 = new FileOutputStream("res/numbers2.txt");
        char letters[] = {'У', 'К', 'Е', 'Н', 'Х', 'В', 'А', 'Р', 'О', 'С', 'М', 'Т'};
        for (int regionCode = 1; regionCode < 100; regionCode++)
        {

            for (int number = 1; number < 1000; number++)
            {
                StringBuilder builder = new StringBuilder();
                for (char firstLetter : letters)
                {
                    for (char secondLetter : letters)
                    {
                        for (char thirdLetter : letters)
                        {
                            builder.append(firstLetter);
                            builder.append(padNumber(number, 3));
                            builder.append(secondLetter);
                            builder.append(thirdLetter);
                            builder.append(padNumber(regionCode, 2));
                            builder.append("\n");
                        }
                    }
                }
                if (number % 2 == 0)
                {
                    writer.write(builder.toString().getBytes());
                }else
                {
                    writer2.write(builder.toString().getBytes());
                }
            }

        }

        writer.flush();
        writer.close();

        System.out.println((System.currentTimeMillis() - start) + " ms");
    }

    private static String padNumber(int number, int numberLength)
    {
        String numberStr = Integer.toString(number);
        StringBuilder result = new StringBuilder();
        result.append("0".repeat(numberLength)).append(numberStr);
        return result.toString();
    }
}
//Тут будет отчёт по проведённой работе
//Стандартное время работы: 24 секунды
//После оптимизации padNumber: 24 секунды
//После оптимизации сборки номеров: 1.3 секунд
//После замены FileOutputStream на PrintWriter: 1.5 секунд (Значит, оставляем FileOutputStream)
//После добавления цикла с regionCode: 56 секунд (по моим наблюдениям, программа работает быстрее, если запись вести для каждого number, а не regionCode)
//При записи в 2 файла: 48 секунд
//При записи в 3 файла: 54 секунды
//При записи в 4 файла: 54 секунды