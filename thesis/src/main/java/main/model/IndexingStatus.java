package main.model;

public enum IndexingStatus
{
    INDEXING,
    INDEXED,
    FAILED
}
