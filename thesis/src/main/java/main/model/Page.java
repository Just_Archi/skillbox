package main.model;

import javax.persistence.*;
import javax.persistence.Index;
import java.util.Objects;
@Entity
@Table(name = "page_table", indexes = @Index(columnList = "path", name = "pathIndex"))
public class Page
{
    @Id
    @Column(nullable = false)
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "path", length = 1023, nullable = false)
    private String path;
    @Column(name = "code", nullable = false)
    private int code;
    @Column(name = "content", length = 16777215, columnDefinition = "mediumtext", nullable = false)
    private String content;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "site_id")
    private Site site;

    public Page(){}

    public Page(String path, int code, String content, Site site)
    {

        this.path = path;
        this.code = code;
        this.content = content;
        this.site = site;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getPath()
    {
        return path;
    }

    public void setPath(String path)
    {
        this.path = path;
    }

    public int getCode()
    {
        return code;
    }

    public void setCode(int code)
    {
        this.code = code;
    }

    public String getContent()
    {
        return content;
    }

    public void setContent(String content)
    {
        this.content = content;
    }

    public Site getSite()
    {
        return site;
    }

    public void setSite(Site site)
    {
        this.site = site;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Page page = (Page) o;
        return id == page.id && code == page.code && Objects.equals(path, page.path) && Objects.equals(content, page.content) && Objects.equals(site, page.site);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(id, path, code, content, site);
    }
}
