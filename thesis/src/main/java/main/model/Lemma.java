package main.model;

import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "lemma_table")
@Transactional
public class Lemma
{
    @Id
    @Column(nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable = false, length = 255)
    private String lemma;

    @Column(nullable = false)
    private int frequency;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "site_id")
    private Site site;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "index_table",
            joinColumns = {@JoinColumn(name = "lemma_id")},
            inverseJoinColumns = {@JoinColumn(name = "page_id")}
    )
    private List<Page> pagesList;

    public Lemma() {}

    public Lemma(String lemma, int frequency, Site site)
    {
        this.lemma = lemma;
        this.frequency = frequency;
        this.site = site;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getLemma()
    {
        return lemma;
    }

    public void setLemma(String lemma)
    {
        this.lemma = lemma;
    }

    public int getFrequency()
    {
        return frequency;
    }

    public void setFrequency(int frequency)
    {
        this.frequency = frequency;
    }

    public Site getSite()
    {
        return site;
    }

    public void setSite(Site site)
    {
        this.site = site;
    }

    public List<Page> getPagesList()
    {
        return pagesList;
    }

    public void setPagesList(List<Page> pagesList)
    {
        this.pagesList = pagesList;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Lemma lemma1 = (Lemma) o;
        return id == lemma1.id && frequency == lemma1.frequency && Objects.equals(lemma, lemma1.lemma) && Objects.equals(site, lemma1.site) && Objects.equals(pagesList, lemma1.pagesList);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(id, lemma, frequency, site, pagesList);
    }
}
