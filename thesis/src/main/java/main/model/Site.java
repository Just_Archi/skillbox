package main.model;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Objects;
import javax.persistence.*;

@Entity
@Table(name = "site_table")
public class Site
{
    @Id
    @Column(nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private IndexingStatus status;

    @Column(nullable = false, length = 8)
    private Timestamp status_time;

    @Column(nullable = false, length = 255)
    private String last_error;

    @Column(nullable = false, length = 255)
    private String url;

    @Column(nullable = false, length = 255)
    private String name;

    public Site(){}

    public Site(String url, String name)
    {
        this.status = IndexingStatus.INDEXED;
        this.status_time = new Timestamp(new Date().getTime());
        this.last_error = "";
        this.url = url;
        this.name = name;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public IndexingStatus getStatus()
    {
        return status;
    }

    public void setStatus(IndexingStatus status) {this.status = status;}

    public Timestamp getStatus_time()
    {
        return status_time;
    }

    public void setStatus_time(Timestamp status_time)
    {
        this.status_time = status_time;
    }

    public String getLast_error()
    {
        return last_error;
    }

    public void setLast_error(String last_error)
    {
        this.last_error = last_error;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Site site = (Site) o;
        return id == site.id && status == site.status && Objects.equals(status_time, site.status_time) && Objects.equals(last_error, site.last_error) && Objects.equals(url, site.url) && Objects.equals(name, site.name);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(id, status, status_time, last_error, url, name);
    }

    @Override
    public String toString()
    {
        return "Site{" +
                "id=" + id +
                ", status=" + status +
                ", status_time=" + status_time +
                ", last_error='" + last_error + '\'' +
                ", url='" + url + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
