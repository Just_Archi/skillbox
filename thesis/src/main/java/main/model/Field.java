package main.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "field_table")
public class Field
{
    @Id
    @Column(nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable = false, length = 255)
    private String name;

    @Column(nullable = false, length = 255)
    private String selector;

    @Column(nullable = false)
    private float weight;

    public Field() {}

    public Field(String name, String selector, float weight)
    {
        this.name = name;
        this.selector = selector;
        this.weight = weight;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getSelector()
    {
        return selector;
    }

    public void setSelector(String selector)
    {
        this.selector = selector;
    }

    public float getWeight()
    {
        return weight;
    }

    public void setWeight(float weight)
    {
        this.weight = weight;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Field field = (Field) o;
        return id == field.id && Float.compare(field.weight, weight) == 0 && Objects.equals(name, field.name) && Objects.equals(selector, field.selector);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(id, name, selector, weight);
    }
}
