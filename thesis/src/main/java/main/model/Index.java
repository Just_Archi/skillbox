package main.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "index_table")
public class Index
{
    @Id
    @Column(nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "page_id", nullable = false)
    private int pageId;

    @Column(name = "lemma_id", nullable = false)
    private int lemmaId;

    @Column(nullable = false)
    private float lemma_rank;

    @Column(nullable = false)
    private int lemma_first_pos;

    public Index() {}

    public Index(int pageId, int lemmaId, float rank, int lemma_first_pos)
    {
        this.pageId = pageId;
        this.lemmaId = lemmaId;
        this.lemma_rank = rank;
        this.lemma_first_pos = lemma_first_pos;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public int getPageId()
    {
        return pageId;
    }

    public void setPageId(int pageId)
    {
        this.pageId = pageId;
    }

    public int getLemmaId()
    {
        return lemmaId;
    }

    public void setLemmaId(int lemmaId)
    {
        this.lemmaId = lemmaId;
    }

    public float getRank()
    {
        return lemma_rank;
    }

    public void setRank(float rank)
    {
        this.lemma_rank = rank;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Index index = (Index) o;
        return id == index.id && pageId == index.pageId && lemmaId == index.lemmaId && Float.compare(index.lemma_rank, lemma_rank) == 0;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(id, pageId, lemmaId, lemma_rank);
    }
}
