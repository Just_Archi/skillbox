package main.service;

import main.repositories.IndexRepository;
import main.model.Lemma;
import main.repositories.LemmaRepository;
import main.model.Page;
import main.repositories.PageRepository;
import main.model.Site;
import main.repositories.SiteRepository;
import main.response.search.SearchResponseEntity;
import main.response.search.SearchResponseItem;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.util.*;


public class SearchService
{
    private LemmaRepository lemmaRepository;
    private IndexRepository indexRepository;
    private PageRepository pageRepository;
    private SiteRepository siteRepository;
    private double highFrequencyPercent;
    private float maxRelAbs = 0;

    public SearchService(double highFrequencyPercent, LemmaRepository lemmaRepository, IndexRepository indexRepository, PageRepository pageRepository, SiteRepository siteRepository)
    {
        this.lemmaRepository = lemmaRepository;
        this.indexRepository = indexRepository;
        this.pageRepository = pageRepository;
        this.siteRepository = siteRepository;
        this.highFrequencyPercent = highFrequencyPercent;
    }

    public SearchResponseEntity search(String query, Site site, int offset, int limit)
    {
        System.out.println("start lemmas");
        Set<String> queryWords = LemmaService.countLemmas(query).keySet();
        System.out.println("start frequency");
        List<Lemma> queryLemmas = (site == null) ? getLemmasFromDB(queryWords) : getLemmasFromSite(queryWords, site);
        System.out.println("start ");
        queryLemmas.sort(Comparator.comparing(Lemma::getFrequency));
        List<Page> pages = null;
        for (Lemma queryLemma : queryLemmas)
        {
            if (pages == null)
            {
                pages = queryLemma.getPagesList(); //TODO: ManyToMany
            } else
            {
                List<Page> currentPages = queryLemma.getPagesList();
                pages.retainAll(currentPages);
            }
        }
        List<SearchResponseItem> items;
        if (pages == null || pages.isEmpty())
        {
            items = new ArrayList<>();
        } else
        {
            items = generateResponseItems(pages, queryLemmas);
        }
        return new SearchResponseEntity(items, offset, limit);
    }

    private List<SearchResponseItem> generateResponseItems(List<Page> responsePages, List<Lemma> queryLemmas)
    {
        List<SearchResponseItem> searchResponseItems = new ArrayList<>();
        List<Integer> lemmaIDs = new ArrayList<>();
        queryLemmas.forEach(lemma -> lemmaIDs.add(lemma.getId()));

        for (Page responsePage : responsePages)
        {
            SearchResponseItem searchResponseItem = new SearchResponseItem();
            Document responseDocument = Jsoup.parse(responsePage.getContent());
            searchResponseItem.setSite(responsePage.getSite().getUrl());
            searchResponseItem.setSiteName(responsePage.getSite().getName());
            searchResponseItem.setUri(responsePage.getPath());
            searchResponseItem.setTitle(responseDocument.title());
            searchResponseItem.setSnippet(generateSnippet(responsePage.getId(), queryLemmas));
            float relAbs = indexRepository.getRelSum(responsePage.getId(), lemmaIDs);
            System.out.println(relAbs);//TODO: clean
            searchResponseItem.setRelevance(relAbs);
            searchResponseItems.add(searchResponseItem);
            if (relAbs > maxRelAbs)
            {
                maxRelAbs = relAbs;
            }
        }
        List<SearchResponseItem> swappedList = swapRelevances(searchResponseItems);
        swappedList.sort(Comparator.comparing(SearchResponseItem::getRelevance));
        return swappedList;
    }

    private List<SearchResponseItem> swapRelevances(List<SearchResponseItem> betaList)
    {
        List<SearchResponseItem> swappedList = new ArrayList<>();
        for (SearchResponseItem beta : betaList)
        {
            SearchResponseItem swapped = new SearchResponseItem(beta.getSite(), beta.getSiteName(), beta.getUri(), beta.getTitle(), beta.getSnippet(), (beta.getRelevance() / maxRelAbs));
            swappedList.add(swapped);
        }
        return swappedList;
    }

    private String generateSnippet(int pageID, List<Lemma> queryLemmas)
    {
        List<Integer> lemmasPos = new ArrayList<>();
        for (Lemma lemma : queryLemmas)
        {
            int pos = indexRepository.getFirstPos(pageID, lemma.getId());
            if (!lemmasPos.contains(pos))
            {
                lemmasPos.add(pos);
            }
        }
        Collections.sort(lemmasPos);
        String pageText = Jsoup.parse(pageRepository.findById(pageID).get().getContent()).text();
        String[] words = pageText.split("\\s+");
        StringBuilder snippetBuilder = new StringBuilder();
        for (int counter = 0; counter < lemmasPos.size(); counter++)
        {
            snippetBuilder.append("\n");
            int pos = lemmasPos.get(counter);
            if (pos < 3)
            {
                for (int i = 0; i < pos; i++)
                {
                    snippetBuilder.append(words[i]).append("\s");
                }
            } else
            {
                snippetBuilder.append("...").append(words[pos - 2]).append("\s").append(words[pos - 1]).append("\s");
            }
            snippetBuilder.append("<b>").append(words[pos]).append("<b/>");
            if (pos > words.length - 4)
            {
                for (int i = pos + 1; i < words.length; i++)
                {
                    snippetBuilder.append("\s").append(words[i]);
                }
            } else
            {
                snippetBuilder.append("\s").append(words[pos + 1]).append("\s").append(words[pos + 2]).append("...");
            }
        }
        return snippetBuilder.toString();
    }

    private List<Lemma> getLemmasFromDB(Set<String> queryWords)
    {
        List<Lemma> queryLemmas = new ArrayList<>();
        for (String q : queryWords)
        {
            Set<Lemma> ls = lemmaRepository.getLemmasByText(q);
            for (Lemma l : ls)
            {
                System.out.println(l.getLemma());
                double highFrequency = pageRepository.countPagesOfSite(l.getSite().getId()) * highFrequencyPercent;
                System.out.println(highFrequency);
                if ((double) l.getFrequency() <= highFrequency)
                {
                    queryLemmas.add(l);
                }
            }
        }
        return queryLemmas;
    }

    private List<Lemma> getLemmasFromSite(Set<String> queryWords, Site site)
    {
        List<Lemma> queryLemmas = new ArrayList<>();
        for (String q : queryWords)
        {
            Lemma l = lemmaRepository.getLemmaByTextAndSiteId(q, site);

            System.out.println(l.getLemma());
            double highFrequency = pageRepository.countPagesOfSite(site.getId()) * highFrequencyPercent;
            System.out.println(highFrequency);
            if ((double) l.getFrequency() <= highFrequency)
            {
                queryLemmas.add(l);
            }

        }
        return queryLemmas;
    }
}
