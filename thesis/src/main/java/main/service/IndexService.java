package main.service;

import main.model.Field;
import main.repositories.FieldRepository;
import main.model.Index;
import main.repositories.IndexRepository;
import main.model.Lemma;
import main.repositories.LemmaRepository;
import main.model.Page;
import main.model.Site;
import main.repositories.SiteRepository;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import java.util.*;

public class IndexService
{
    private FieldRepository fieldRepository;
    private IndexRepository indexRepository;
    private LemmaRepository lemmaRepository;
    private SiteRepository siteRepository;
    private Iterable<Field> fields;
    private int batchVolume;

    public IndexService(int batchVolume, FieldRepository fieldRepository, IndexRepository indexRepository, LemmaRepository lemmaRepository, SiteRepository siteRepository)
    {
        this.batchVolume = batchVolume;
        this.fieldRepository = fieldRepository;
        this.indexRepository = indexRepository;
        this.lemmaRepository = lemmaRepository;
        this.siteRepository = siteRepository;
        fields = fieldRepository.findAll();
    }

    public void indexPages(List<Page> pages)
    {
        for (Page page : pages)
        {
            indexPage(page);
        }
    }

    public void indexPage(Page page)
    {
        if (page.getCode() < 400)
        {
            analyzePage(page);
        }
    }

    private void analyzePage(Page page)
    {
        Map<String, Float> ranks = new HashMap<>();
        Set<String> lemmas = new HashSet<>();
        for (Field field : fields)
        {
            Document doc = Jsoup.parse(page.getContent());
            Element block = doc.selectFirst(field.getSelector());
            if (block != null)
            {
                Map<String, Integer> blockLemmas = LemmaService.countLemmas(block.text());
                for (Map.Entry<String, Integer> entry : blockLemmas.entrySet())
                {
                    String key = entry.getKey();
                    Integer value = entry.getValue();
                    lemmas.add(key);
                    if (ranks.containsKey(key))
                    {
                        ranks.put(key, (ranks.get(key) + value * field.getWeight()));
                    } else
                    {
                        ranks.put(key, (value * field.getWeight()));
                    }
                }
            }
        }
        List<Lemma> savedLemmas = saveLemmas(lemmas, page.getSite());
        List<Index> indexesToSave = new ArrayList<>();
        Map<String, Integer> lemmasFirstPos = LemmaService.findLemmasFirstPos(Jsoup.parse(page.getContent()).text());
        for (Lemma savedLemma : savedLemmas)
        {
            int firstPos = lemmasFirstPos.get(savedLemma.getLemma());
            Index index = new Index(page.getId(), savedLemma.getId(), ranks.get(savedLemma.getLemma()), firstPos);
            indexesToSave.add(index);
        }
        batchSaveIndexes(indexesToSave);

        savedLemmas = null;
        indexesToSave = null;
        lemmasFirstPos = null;
    }

    private List<Lemma> saveLemmas(Set<String> lemmaTexts, Site site)
    {
        List<Lemma> lemmasForSave = new ArrayList<>();
        for (String lemmaText : lemmaTexts)
        {
            Lemma currentLemma = lemmaRepository.getLemmaFromSite(lemmaText, site.getId());
            if (currentLemma == null)
            {
                currentLemma = new Lemma(lemmaText, 1, site);
            } else
            {
                currentLemma.setFrequency(currentLemma.getFrequency() + 1);
            }
            lemmasForSave.add(currentLemma);
        }
        return batchSaveLemmas(lemmasForSave);
    }

    private List<Lemma> batchSaveLemmas(List<Lemma> lemmas)
    {
        List<Lemma> savedLemmas = new ArrayList<>();
        int counter = 0;
        List<Lemma> batch = new ArrayList<>();
        for (Lemma lemma : lemmas)
        {
            batch.add(lemma);
            counter++;
            if (counter >= batchVolume)
            {
                Iterable<Lemma> lemmaIterable = lemmaRepository.saveAll(batch);
                lemmaIterable.forEach(savedLemmas::add);
                batch.clear();
                counter = 0;
            }
        }
        Iterable<Lemma> lemmaIterable = lemmaRepository.saveAll(batch);
        lemmaIterable.forEach(savedLemmas::add);
        return savedLemmas;
    }

    private void batchSaveIndexes(List<Index> indexes)
    {
        int counter = 0;
        List<Index> batch = new ArrayList<>();
        for (Index index : indexes)
        {
            batch.add(index);
            counter++;
            if (counter >= batchVolume)
            {
                indexRepository.saveAll(batch);
                batch.clear();
                counter = 0;
            }
        }
        indexRepository.saveAll(batch);
    }
}
