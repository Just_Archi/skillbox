package main.service;

import org.apache.lucene.morphology.LuceneMorphology;
import org.apache.lucene.morphology.english.EnglishLuceneMorphology;
import org.apache.lucene.morphology.russian.RussianLuceneMorphology;
import org.jsoup.Jsoup;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LemmaService
{
    private static final LuceneMorphology engMorph;
    private static final LuceneMorphology rusMorph;

    static
    {
        try
        {
            engMorph = new EnglishLuceneMorphology();
            rusMorph = new RussianLuceneMorphology();
        } catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }


    public static Map<String, Integer> countLemmas(String str)
    {
        Map<String, Integer> lemmas = new HashMap<>();
        String[] words = Jsoup.parse(str).text().toLowerCase().split("\\s+");
        for (String word : words)
        {
            List<String> adds = defineWordLemmas(word);
            for (String add : adds)
            {
                if (lemmas.containsKey(add))
                {
                    int count = lemmas.get(add) + 1;
                    lemmas.put(add, count);
                } else
                {
                    lemmas.put(add, 1);
                }
            }
        }
        return lemmas;
    }

    public static Map<String, Integer> findLemmasFirstPos(String text)
    {
        Map<String, Integer> lemmasFirstPos = new HashMap<>();
        String[] words = Jsoup.parse(text).text().toLowerCase().split("\\s+"); //TODO: .replaceAll("[^�-�a-z\\s]", "\s")
        for (int i = 0; i < words.length; i++)
        {
            String word = words[i];
            //word = word.toLowerCase().replaceAll("[^�-�a-z\\s]", "");
            if (word.length() > 0)
            {
                //System.out.println(word); //@TODO: clean
                List<String> adds = defineWordLemmas(word);
                for (String add : adds)
                {
                    if (!lemmasFirstPos.containsKey(add))
                    {
                        lemmasFirstPos.put(add, i);
                    }
                }
            }
        }
        return lemmasFirstPos;
    }

    public static List<String> defineWordLemmas(String word)
    {
        List<String> lemmas = new ArrayList<>();
        if (word.matches("[a-z]+") && word.length() != 0 && engCheckType(word))
        {
            List<String> wordBaseForms = engMorph.getMorphInfo(word);
            wordBaseForms.forEach(form -> lemmas.add(form.substring(0, form.indexOf("\s") - 2)));


        } else if (word.matches("[�-�]+") && word.length() != 0 && rusCheckType(word))
        {
            List<String> wordBaseForms = rusMorph.getMorphInfo(word);
            wordBaseForms.forEach(form -> lemmas.add(form.substring(0, form.indexOf("\s") - 2)));

        } else if (word.matches("\\d+") && word.length() != 0)
        {
            lemmas.add(word);
        }
        return lemmas;
    }

    public static boolean rusCheckType(String form)
    {
        String[] types = {"����", "�����", "����", "����"};
        String type = form.substring(form.lastIndexOf("\s") + 1);
        for (String currentType : types)
        {
            if (type.equals(currentType))
            {
                return false;
            }
        }
        return true;
    }

    public static boolean engCheckType(String form)
    {
        String[] types = {"CONJ", "PREP", "PART", "INT"};
        String type = form.substring(form.lastIndexOf("\s") + 1);
        for (String currentType : types)
        {
            if (type.equals(currentType))
            {
                return false;
            }
        }
        return true;
    }
}
