package main.service.net;

import main.repositories.FieldRepository;
import main.repositories.IndexRepository;
import main.repositories.LemmaRepository;
import main.repositories.PageRepository;
import main.model.Site;
import main.repositories.SiteRepository;
import main.service.IndexService;

public class NetService
{
    private static NetService instance;
    private final int coolDownMillis;
    private final PageRepository pageRepository;
    private final FieldRepository fieldRepository;
    private final IndexRepository indexRepository;
    private final LemmaRepository lemmaRepository;
    private final SiteRepository siteRepository;
    private final IndexService indexService;
    private final String userAgent;
    private final int batchVolume;

    private NetService(int coolDownMillis, int batchVolume, String userAgent, PageRepository pageRepository, FieldRepository fieldRepository, IndexRepository indexRepository, LemmaRepository lemmaRepository, SiteRepository siteRepository, IndexService indexService)
    {
        this.coolDownMillis = coolDownMillis;
        this.batchVolume = batchVolume;
        this.userAgent = userAgent;
        this.pageRepository = pageRepository;
        this.fieldRepository = fieldRepository;
        this.indexRepository = indexRepository;
        this.lemmaRepository = lemmaRepository;
        this.siteRepository = siteRepository;
        this.indexService = indexService;
    }

    public static void setInstance(int coolDownMillis, int batchVolume, String userAgent, PageRepository pageRepository, FieldRepository fieldRepository, IndexRepository indexRepository, LemmaRepository lemmaRepository, SiteRepository siteRepository, IndexService indexService)
    {
        instance = new NetService(coolDownMillis, batchVolume, userAgent, pageRepository, fieldRepository, indexRepository, lemmaRepository, siteRepository, indexService);
    }

    public static NetService getInstance()
    {
        return instance;
    }

    public PagesCollector generatePagesCollector(Site site)
    {
        return new PagesCollector(site, pageRepository, siteRepository, indexService, coolDownMillis, userAgent);
    }
}
