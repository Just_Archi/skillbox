package main.service.net;

import main.Main;
import main.model.IndexingStatus;
import main.model.Page;
import main.repositories.PageRepository;
import main.model.Site;
import main.repositories.SiteRepository;
import main.service.IndexService;
import org.jsoup.Connection;
import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.UnsupportedMimeTypeException;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.RecursiveAction;

public class PagesCollector extends RecursiveAction
{

    private String currentRef;
    private Site site;
    private long coolDownMillis;
    private PageRepository pageRepository;
    private SiteRepository siteRepository;
    private IndexService indexService;
    private String userAgent;
    private static final Map<Integer, Set<String>> addedLinks = new ConcurrentHashMap<>();
    private static Map<Integer, Long> lastTaskTimestamps = new ConcurrentHashMap<>();

    PagesCollector(Site site, PageRepository pageRepository, SiteRepository siteRepository, IndexService indexService, int coolDownMillis, String userAgent)
    {
        currentRef = "/";
        this.site = site;
        this.pageRepository = pageRepository;
        this.siteRepository = siteRepository;
        this.indexService = indexService;
        this.coolDownMillis = coolDownMillis;
        this.userAgent = userAgent;
        lastTaskTimestamps.put(site.getId(), System.currentTimeMillis());
    }

    private PagesCollector(String currentRef, Site site, PageRepository pageRepository, SiteRepository siteRepository, IndexService indexService, long coolDownMillis, String userAgent)
    {
        this.currentRef = currentRef;
        this.site = siteRepository.findById(site.getId()).get();
        this.pageRepository = pageRepository;
        this.siteRepository = siteRepository;
        this.indexService = indexService;
        this.coolDownMillis = coolDownMillis;
        this.userAgent = userAgent;
    }


    @Override
    protected void compute()
    {
        if (site.getStatus() == IndexingStatus.FAILED)
        {
            return;
        }
        if (Objects.equals(currentRef, "/"))
        {
            addedLinks.put(site.getId(), ConcurrentHashMap.newKeySet());
            addedLinks.get(site.getId()).add(currentRef);
        }
        Page page;
        synchronized (Main.class)
        {
            long currentCoolDown = coolDownMillis - (System.currentTimeMillis() - lastTaskTimestamps.get(site.getId()));
            if (currentCoolDown > 0)
            {
                try
                {
                    Thread.sleep(currentCoolDown);
                }
                catch (InterruptedException e){}
            }
            page = createPage(currentRef);
            lastTaskTimestamps.put(site.getId(), System.currentTimeMillis());
        }
        System.out.println(site.getUrl()+currentRef);//TODO: clean
        Document document = Jsoup.parse(page.getContent(), (site.getUrl() + currentRef));
        Elements elements = document.select("a");
        for (Element element : elements)
        {
            String taskRef = element.attr("href");
            if (!(addedLinks.get(site.getId()).contains(taskRef) || taskRef.contains("http://") || taskRef.contains("https://")))
            {
                addedLinks.get(site.getId()).add(taskRef);
                PagesCollector task = new PagesCollector(taskRef, site, pageRepository, siteRepository, indexService, coolDownMillis, userAgent);
                task.fork();
                task.join();
            }
        }
        indexPage(page);
        siteRepository.updateTime(site.getId());
    }

    public void addPage(String pageRef)
    {
        Page page = createPage(pageRef);
        indexPage(page);
    }

    private void indexPage(Page page)
    {
        Page savedPage = pageRepository.save(page);
        System.out.println("saved");
        indexService.indexPage(savedPage);
        System.out.println("indexed");
        page = null;
        savedPage = null;
    }


    private Page createPage(String currentRef)
    {
        Page page;
        String fullRef = site.getUrl() + currentRef;
        try
        {
            Connection.Response response = Jsoup.connect(fullRef)
                    .userAgent(userAgent) //"Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:25.0) Gecko/20100101 Firefox/25.0"
                    .referrer("http://www.google.com").execute();
            int statusCode = response.statusCode();
            Document document = response.parse();
            String content = document.toString();
            page = new Page(currentRef, statusCode, content, site);
        } catch (UnsupportedMimeTypeException unsupportedMimeTypeException)
        {
            page = new Page(currentRef, 200, unsupportedMimeTypeException.getMimeType(), site);
        } catch (HttpStatusException httpStatusException)
        {
            page = new Page(currentRef, httpStatusException.getStatusCode(), "null", site);
        } catch (IllegalArgumentException illegalArgumentException)
        {
            page = new Page(currentRef, 403, "null", site);
        } catch (IOException e)
        {
            page = new Page("null", 600, "null", site);
            e.printStackTrace();
        }

        return page;
    }
}
