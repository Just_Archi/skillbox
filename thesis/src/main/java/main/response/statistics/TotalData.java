package main.response.statistics;

import main.repositories.LemmaRepository;
import main.repositories.PageRepository;
import main.repositories.SiteRepository;

public class TotalData
{
    private int sites;
    private int pages;
    private int lemmas;
    private boolean indexing;

    TotalData(LemmaRepository lr, SiteRepository sr, PageRepository pr)
    {
        sites = sr.countSites();
        pages = pr.countPages();
        lemmas = lr.countLemmas();
        indexing = (sr.countIndexingSites() > 0);
    }

    public TotalData()
    {
    }

    public int getSites()
    {
        return sites;
    }

    public void setSites(int sites)
    {
        this.sites = sites;
    }

    public int getPages()
    {
        return pages;
    }

    public void setPages(int pages)
    {
        this.pages = pages;
    }

    public int getLemmas()
    {
        return lemmas;
    }

    public void setLemmas(int lemmas)
    {
        this.lemmas = lemmas;
    }

    public boolean isIndexing()
    {
        return indexing;
    }

    public void setIndexing(boolean indexing)
    {
        this.indexing = indexing;
    }
}
