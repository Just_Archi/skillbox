package main.response.statistics;

import main.model.IndexingStatus;
import main.model.Site;
import main.repositories.LemmaRepository;
import main.repositories.PageRepository;

import java.sql.Timestamp;

public class DetailedItemData
{
    private String url;
    private String name;
    private String status;
    private long statusTime;
    private String error;
    private int pages;
    private int lemmas;

    DetailedItemData(Site site, PageRepository pr, LemmaRepository lr)
    {
        this.url = site.getUrl();
        this.name = site.getName();
        this.status = site.getStatus().toString();
        this.statusTime = site.getStatus_time().getTime();
        this.error = site.getLast_error();
        this.pages = pr.countPagesOfSite(site.getId());
        this.lemmas = lr.countLemmasOfSite(site.getId());
    }

    public DetailedItemData()
    {
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public long getStatusTime()
    {
        return statusTime;
    }

    public void setStatusTime(long statusTime)
    {
        this.statusTime = statusTime;
    }

    public String getError()
    {
        return error;
    }

    public void setError(String error)
    {
        this.error = error;
    }

    public int getPages()
    {
        return pages;
    }

    public void setPages(int pages)
    {
        this.pages = pages;
    }

    public int getLemmas()
    {
        return lemmas;
    }

    public void setLemmas(int lemmas)
    {
        this.lemmas = lemmas;
    }
}
