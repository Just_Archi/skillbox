package main.response.statistics;

import main.repositories.LemmaRepository;
import main.repositories.PageRepository;
import main.repositories.SiteRepository;

public class StatisticsResponseEntity
{
    private boolean result = true;
    private StatisticsData statistics;

    public StatisticsResponseEntity(SiteRepository sr, PageRepository pr, LemmaRepository lr)
    {
        this.statistics = new StatisticsData(sr, lr, pr);
    }

    public StatisticsResponseEntity()
    {
    }

    public boolean isResult()
    {
        return result;
    }

    public void setResult(boolean result)
    {
        this.result = result;
    }

    public StatisticsData getStatistics()
    {
        return statistics;
    }

    public void setStatistics(StatisticsData statistics)
    {
        this.statistics = statistics;
    }
}
