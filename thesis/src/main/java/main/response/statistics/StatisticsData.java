package main.response.statistics;

import main.model.Site;
import main.repositories.LemmaRepository;
import main.repositories.PageRepository;
import main.repositories.SiteRepository;

import java.util.ArrayList;
import java.util.List;

public class StatisticsData
{
    private TotalData total;
    private List<DetailedItemData> detailed;

    StatisticsData(SiteRepository sr, LemmaRepository lr, PageRepository pr)
    {
        total = new TotalData(lr, sr, pr);
        detailed = new ArrayList<>();
        Iterable<Site> sites = sr.findAll();
        for (Site site : sites)
        {
            DetailedItemData detailedItemData = new DetailedItemData(site, pr, lr);
            detailed.add(detailedItemData);
        }
    }

    public StatisticsData()
    {
    }

    public TotalData getTotal()
    {
        return total;
    }

    public void setTotal(TotalData total)
    {
        this.total = total;
    }

    public List<DetailedItemData> getDetailed()
    {
        return detailed;
    }

    public void setDetailed(List<DetailedItemData> detailed)
    {
        this.detailed = detailed;
    }
}
