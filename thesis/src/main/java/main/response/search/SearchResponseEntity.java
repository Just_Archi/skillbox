package main.response.search;

import java.util.ArrayList;
import java.util.List;

public class SearchResponseEntity
{
    private boolean result = true;
    int count;
    List<SearchResponseItem> data;

    public SearchResponseEntity(List<SearchResponseItem> data, int offset, int limit)
    {
        this.count = data.size();
        this.data = new ArrayList<>();
        for (int i = offset; i < offset+limit; i++)
        {
            if (i == data.size())
            {
                break;
            }
            this.data.add(data.get(i));
        }
    }

    public SearchResponseEntity()
    {
    }

    public boolean isResult()
    {
        return result;
    }

    public void setResult(boolean result)
    {
        this.result = result;
    }

    public int getCount()
    {
        return count;
    }

    public void setCount(int count)
    {
        this.count = count;
    }

    public List<SearchResponseItem> getData()
    {
        return data;
    }

    public void setData(List<SearchResponseItem> data)
    {
        this.data = data;
    }
}
