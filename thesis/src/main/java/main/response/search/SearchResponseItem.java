package main.response.search;

import java.util.Objects;

public class SearchResponseItem
{
    private String site;
    private String siteName;
    private String uri;
    private String title;
    private String snippet;
    private float relevance;

    public SearchResponseItem(){}

    public SearchResponseItem(String site, String siteName, String uri, String title, String snippet, float relevance)
    {
        this.site = site;
        this.siteName = siteName;
        this.uri = uri;
        this.title = title;
        this.snippet = snippet;
        this.relevance = relevance;
    }

    public String getUri()
    {
        return uri;
    }

    public void setUri(String uri)
    {
        this.uri = uri;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getSnippet()
    {
        return snippet;
    }

    public void setSnippet(String snippet)
    {
        this.snippet = snippet;
    }

    public float getRelevance()
    {
        return relevance;
    }

    public void setRelevance(float relevance)
    {
        this.relevance = relevance;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SearchResponseItem that = (SearchResponseItem) o;
        return Float.compare(that.relevance, relevance) == 0 && Objects.equals(uri, that.uri) && Objects.equals(title, that.title) && Objects.equals(snippet, that.snippet);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(uri, title, snippet, relevance);
    }

    @Override
    public String toString()
    {
        return "SearchResponseEntity{" +
                "uri='" + uri + '\'' +
                ", title='" + title + '\'' +
                ", snippet='" + snippet + '\'' +
                ", relevance=" + relevance +
                '}';
    }

    public String getSite()
    {
        return site;
    }

    public void setSite(String site)
    {
        this.site = site;
    }

    public String getSiteName()
    {
        return siteName;
    }

    public void setSiteName(String siteName)
    {
        this.siteName = siteName;
    }
}
