package main.response.basic;

public class ErrorResponseEntity
{
    private boolean result = false;

    private String error;

    public ErrorResponseEntity(String error)
    {
        this.error = error;
    }

    public ErrorResponseEntity()
    {
    }

    public boolean isResult()
    {
        return result;
    }

    public void setResult(boolean result)
    {
        this.result = result;
    }

    public String getError()
    {
        return error;
    }

    public void setError(String error)
    {
        this.error = error;
    }
}
