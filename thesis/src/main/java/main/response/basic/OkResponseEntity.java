package main.response.basic;

public class OkResponseEntity
{
    private boolean result = true;

    public OkResponseEntity()
    {
    }

    public boolean isResult()
    {
        return result;
    }

    public void setResult(boolean result)
    {
        this.result = result;
    }
}
