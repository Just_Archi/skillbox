package main.controller;

import main.config.CustomConfig;
import main.model.Field;
import main.model.IndexingStatus;
import main.model.Site;
import main.repositories.*;
import main.response.basic.ErrorResponseEntity;
import main.response.basic.OkResponseEntity;
import main.response.statistics.StatisticsResponseEntity;
import main.service.IndexService;
import main.service.SearchService;
import main.service.net.NetService;
import main.service.net.PagesCollector;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.*;
import java.util.concurrent.ForkJoinPool;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
@RequestMapping("/api")
public class ResponseController
{
    private CustomConfig config;
    private PageRepository pageRepository;
    private FieldRepository fieldRepository;
    private IndexRepository indexRepository;
    private LemmaRepository lemmaRepository;
    private SiteRepository siteRepository;

    private NetService netService;
    private IndexService indexService;
    private SearchService searchService;

    private final Field title = new Field("title", "title", 1);
    private final Field body = new Field("body", "body", (float) 0.8);
    Pattern pagePattern = Pattern.compile("(https?://[a-zA-Z0-9\\.]+)(/.*)");
    public ResponseController(CustomConfig config, PageRepository pageRepository, FieldRepository fieldRepository, IndexRepository indexRepository, LemmaRepository lemmaRepository, SiteRepository siteRepository)
    {
        this.config = config;
        this.pageRepository = pageRepository;
        this.fieldRepository = fieldRepository;
        this.indexRepository = indexRepository;
        this.lemmaRepository = lemmaRepository;
        this.siteRepository = siteRepository;

        fieldRepository.save(title);
        fieldRepository.save(body);

        indexService = new IndexService(config.getBatchVolume(), fieldRepository, indexRepository, lemmaRepository, siteRepository);

        NetService.setInstance(config.getCoolDownMillis(), config.getBatchVolume(), config.getUserAgent(), pageRepository, fieldRepository, indexRepository, lemmaRepository, siteRepository, indexService);
        netService = NetService.getInstance();
        searchService = new SearchService(config.getHighFrequencyPercent(), lemmaRepository, indexRepository, pageRepository, siteRepository);
    }

    @GetMapping("/statistics")
    public ResponseEntity<StatisticsResponseEntity> statistics()
    {
        StatisticsResponseEntity statistics = new StatisticsResponseEntity(siteRepository, pageRepository, lemmaRepository);
        return ResponseEntity.ok(statistics);
    }

    @GetMapping("/startIndexing")
    public ResponseEntity startIndexing()
    {
        if (siteRepository.countIndexingSites() > 0)
        {
            return ResponseEntity.ok(new ErrorResponseEntity("���������� ��� ��������"));
        }
        siteRepository.set();
        siteRepository.truncate();
        pageRepository.truncate();
        lemmaRepository.truncate();
        indexRepository.truncate();

        List<Site> savedSites = new ArrayList<>();
        for (Site site: config.getSites())
        {
            site.setStatus(IndexingStatus.INDEXING);
            site.setStatus_time(new Timestamp(new Date().getTime()));
            site.setLast_error("");

            savedSites.add(siteRepository.save(site));
        }
        for (Site site : savedSites)
        {
            PagesCollector collector = netService.generatePagesCollector(site);
            Thread thread = new Thread(() ->
            {
                ForkJoinPool pool = new ForkJoinPool();
                pool.invoke(collector);
                if (pageRepository.countPagesOfSite(site.getId()) == 0)
                {
                    siteRepository.setErrorText(site.getId(), "�� ������� ������������");
                    siteRepository.updateStatus(site.getId(), IndexingStatus.FAILED);
                }
                else
                {
                    siteRepository.updateStatus(site.getId(), IndexingStatus.INDEXED);
                }
            });

            thread.start();
        }
        return ResponseEntity.ok(new OkResponseEntity());
    }

    @GetMapping("/stopIndexing")
    public ResponseEntity stopIndexing()
    {
        if (siteRepository.countIndexingSites() == 0)
        {
            return ResponseEntity.ok(new ErrorResponseEntity("���������� ��� ���������"));
        }
        for (Site site : siteRepository.findAll())
        {
            siteRepository.setErrorText(site.getId(), "���������� ����������� �������������");
            siteRepository.updateStatus(site.getId(), IndexingStatus.FAILED);
        }
        return ResponseEntity.ok(new OkResponseEntity());
    }

    @PostMapping("/indexPage")
    public ResponseEntity indexPage(@RequestParam(required = false) String url)
    {
        Matcher pageMatcher = pagePattern.matcher(url);
        boolean match = pageMatcher.find();
        System.out.println(match);
        if (!match || url == null)
        {
            return ResponseEntity.ok(new ErrorResponseEntity("������ ����� ������ ��������"));
        }
        String siteURL = pageMatcher.group(1);
        String pageURL = pageMatcher.group(2);
        Site site = siteRepository.findByURL(siteURL);
        if (site == null)
        {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ErrorResponseEntity("������ ���� ����������� � ����� ������������"));
        }
        netService.generatePagesCollector(site).addPage(pageURL);
        return ResponseEntity.ok(new OkResponseEntity());
    }

    @GetMapping("/search")
    public ResponseEntity search(@RequestParam(required = false) String query, @RequestParam(required = false) String site, @RequestParam(defaultValue = "0") int offset, @RequestParam(defaultValue = "20") int limit)
    {
        if (query == null)
        {
            return ResponseEntity.ok(new ErrorResponseEntity("����� ������ ��������� ������"));
        }
        Site siteFromString = siteRepository.findByURL(site);
        return ResponseEntity.ok(searchService.search(query, siteFromString, offset, limit));
    }
}
