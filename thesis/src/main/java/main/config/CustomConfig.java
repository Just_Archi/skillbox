package main.config;


import main.model.Site;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

//TODO: add configuration
@Configuration
@ConfigurationProperties("custom-config")
public class CustomConfig
{
    private static final String COMMON_WEB_INTERFACE_WAY = "/admin";
    private List<Site> sites;
    private String userAgent;
    private String webInterfaceWay;
    private Integer batchVolume;
    private Double highFrequencyPercent;

    private Integer coolDownMillis;

    public CustomConfig()
    {
    }

    public List<Site> getSites()
    {
        return sites;
    }

    public void setSites(List<Site> sites)
    {
        this.sites = sites;
    }

    public String getUserAgent()
    {
        return userAgent;
    }

    public void setUserAgent(String userAgent)
    {
        this.userAgent = userAgent;
    }

    public String getWebInterfaceWay()
    {
        return (webInterfaceWay == null) ? COMMON_WEB_INTERFACE_WAY : webInterfaceWay;
    }

    public void setWebInterfaceWay(String webInterfaceWay)
    {
        this.webInterfaceWay = webInterfaceWay;
    }

    public Integer getBatchVolume()
    {
        return batchVolume;
    }

    public void setBatchVolume(Integer batchVolume)
    {
        this.batchVolume = batchVolume;
    }

    public Double getHighFrequencyPercent()
    {
        return highFrequencyPercent;
    }

    public void setHighFrequencyPercent(Double highFrequencyPercent)
    {
        this.highFrequencyPercent = highFrequencyPercent;
    }

    public Integer getCoolDownMillis()
    {
        return coolDownMillis;
    }

    public void setCoolDownMillis(Integer coolDownMillis)
    {
        this.coolDownMillis = coolDownMillis;
    }
}
