package main.repositories;


import main.model.IndexingStatus;
import main.model.Site;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


import java.sql.Timestamp;
import java.util.Date;


@Repository
public interface SiteRepository extends CrudRepository<Site, Integer>
{
    @Transactional
    @Modifying
    @Deprecated
    @Query(value = "UPDATE site_table SET status_time = ?2 WHERE id = ?1", nativeQuery = true)
    void changeTime(int siteID, Timestamp dateTime);

    default void updateTime(int siteID)
    {
        changeTime(siteID, new Timestamp(new Date().getTime()));
    }

    @Transactional
    @Modifying
    @Deprecated
    @Query(value = "UPDATE site_table SET status_time = ?2, status = ?3 WHERE id = ?1", nativeQuery = true)
    void changeStatus(int siteID, Timestamp dateTime, String status);

    default void updateStatus(int siteID, IndexingStatus status)
    {
        changeStatus(siteID, new Timestamp(new Date().getTime()), status.toString());
    }

    @Transactional
    @Modifying
    @Deprecated
    @Query(value = "UPDATE site_table SET status_time = ?2, last_error = ?3 WHERE id = ?1", nativeQuery = true)
    void changeErrorText(int siteID, Timestamp dateTime, String error);

    default void setErrorText(int siteID, String text)
    {
        changeErrorText(siteID, new Timestamp(new Date().getTime()), text);
    }

    @Transactional
    @Query(value = "SELECT COUNT(*) FROM site_table", nativeQuery = true)
    Integer countSites();

    @Transactional
    @Query(value = "select COUNT(*) from site_table where status = 'INDEXING'", nativeQuery = true)
    Integer countIndexingSites();

    @Transactional
    @Modifying
    @Query(value = "TRUNCATE TABLE site_table", nativeQuery = true)
    void truncate();

    @Transactional
    @Query(value = "SELECT * FROM site_table WHERE url = ?1 LIMIT 1", nativeQuery = true)
    Site findByURL(String url);

    @Transactional
    @Modifying
    @Query(value = "SET FOREIGN_KEY_CHECKS = 0", nativeQuery = true)
    void set();
}
