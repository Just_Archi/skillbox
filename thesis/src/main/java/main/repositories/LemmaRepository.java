package main.repositories;

import main.model.Lemma;
import main.model.Site;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;


@Repository
public interface LemmaRepository extends CrudRepository<Lemma, Integer>
{
    @Transactional
    @Query(value = "SELECT l FROM Lemma l JOIN FETCH l.pagesList WHERE l.lemma = ?1")
    Set<Lemma> getLemmasByText(String text);
    @Transactional
    @Query(value = "SELECT l FROM Lemma l JOIN FETCH l.pagesList WHERE l.lemma = ?1 AND l.site = ?2")
    Lemma getLemmaByTextAndSiteId(String text, Site site);
    @Transactional
    @Query(value = "SELECT * FROM lemma_table l WHERE l.lemma = ?1 AND l.site_id = ?2 LIMIT 1", nativeQuery = true)
    Lemma getLemmaFromSite(String text, int siteID);

    @Transactional
    @Query(value = "SELECT COUNT(*) FROM lemma_table WHERE site_id = ?1", nativeQuery = true)
    Integer countLemmasOfSite(int siteID);
    @Transactional
    @Query(value = "SELECT COUNT(*) FROM lemma_table", nativeQuery = true)
    Integer countLemmas();

    @Transactional
    @Modifying
    @Query(value = "TRUNCATE TABLE lemma_table", nativeQuery = true)
    void truncate();
}
