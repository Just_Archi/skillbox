package main.repositories;

import main.model.Page;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface PageRepository extends CrudRepository<Page, Integer>
{
    @Transactional
    @Query(value = "SELECT COUNT(*) FROM page_table WHERE site_id = ?1", nativeQuery = true)
    Integer countPagesOfSite(int siteID);
    @Transactional
    @Query(value = "SELECT COUNT(*) FROM page_table", nativeQuery = true)
    Integer countPages();

    @Transactional
    @Modifying
    @Query(value = "TRUNCATE TABLE page_table", nativeQuery = true)
    void truncate();
}