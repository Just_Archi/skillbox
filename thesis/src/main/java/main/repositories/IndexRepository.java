package main.repositories;

import main.model.Index;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

@Repository
public interface IndexRepository extends CrudRepository<Index, Integer>
{
    @Transactional
    @Query(value = "SELECT page_id FROM index_table WHERE lemma_id = ?1 GROUP BY page_id", nativeQuery = true)
    Set<Integer> getPagesByLemmaID(Integer lemmaID);

    @Transactional
    @Query(value = "SELECT lemma_rank FROM index_table WHERE page_id = ?1 AND lemma_id = ?2", nativeQuery = true)
    Float getRel(Integer pageID, Integer lemmaID);

    @Transactional
    @Query(value = "SELECT SUM(lemma_rank) FROM index_table WHERE page_id = ?1 AND lemma_id IN ?2", nativeQuery = true)
    Float getRelSum(Integer pageID, List<Integer> lemmaIDs);

    @Transactional
    @Query(value = "SELECT lemma_first_pos FROM index_table WHERE page_id = ?1 AND lemma_id = ?2", nativeQuery = true)
    Integer getFirstPos(Integer pageID, Integer lemmaID);

    @Transactional
    @Modifying
    @Query(value = "TRUNCATE TABLE index_table", nativeQuery = true)
    void truncate();
}
