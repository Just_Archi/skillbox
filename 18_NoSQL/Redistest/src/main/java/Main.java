public class Main
{
    public static void main(String[] args) throws InterruptedException
    {
        RedisSite.init();
        while (true)
        {
            if (Math.random() >= 0.9)
            {
                RedisSite.pay(getRandomId());
            }
            RedisSite.printNext();
            Thread.sleep(1000);
        }
    }

    public static String getRandomId()
    {
        return Integer.toString((int) (2*(Math.random()*10) + 1));
    }
}
