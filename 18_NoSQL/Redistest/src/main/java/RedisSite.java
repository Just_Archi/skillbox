import org.redisson.Redisson;
import org.redisson.api.RScoredSortedSet;
import org.redisson.api.RedissonClient;
import org.redisson.client.RedisConnectionException;
import org.redisson.config.Config;

import java.util.ArrayList;

public class RedisSite
{
    private static RedissonClient redisClient;
    private static RScoredSortedSet<String> users;

    public static void init()
    {
        Config config = new Config();
        config.useSingleServer().setAddress("redis://127.0.0.1:6379");
        try {
            redisClient = Redisson.create(config);
        } catch (RedisConnectionException Exc) {
            System.out.println("Не удалось подключиться к Redis");
            System.out.println(Exc.getMessage());
        }

        users = redisClient.getScoredSortedSet("users");

        for (int index = 1; index < 21; index++)
        {
            users.add(index, String.valueOf(index));
        }
    }

    public static void printNext()
    {
        String firstUser = users.pollFirst();
        System.out.println("На главной странице выводим пользователя " + firstUser);
        ArrayList<String> remainUserList = (ArrayList<String>) users.readAll();
        for (String user : remainUserList)
        {
            users.add((users.getScore(user)-1), user);
        }
        users.add(20, firstUser);
    }

    public static void pay(String id)
    {
        System.out.println("Пользователь " + id + " оплатил услугу");
        Double index = users.getScore(id);
        users.remove(id);
        ArrayList<String> remainUserList = (ArrayList<String>) users.readAll();
        for (String user : remainUserList)
        {
            Double currentIndex = users.getScore(user);
            if (currentIndex < index)
            {
                users.add((currentIndex + 1), user);
            }
        }
        users.add(1, id);
    }
}
