import com.mongodb.MongoClient;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import static com.mongodb.client.model.Filters.*;
import static com.mongodb.client.model.Sorts.*;

import com.mongodb.client.model.Accumulators;
import com.mongodb.client.model.Aggregates;
import org.bson.Document;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main
{
    private static Path path = Path.of("src/main/resources/mongo.csv");
    private static MongoCollection<Document> collection;
    public static void main(String[] args) throws Exception
    {
        MongoClient mongoClient = new MongoClient("localhost", 27017);
        MongoDatabase database = mongoClient.getDatabase("local");
        collection = database.getCollection("Students");
        collection.drop();
        parseCSV();
        System.out.println("Кол-во студентов = " + collection.countDocuments());
        System.out.println("Старше 40 лет = " + collection.countDocuments(gt("age", 40)));
        MongoCursor<Document> theYoungestCursor = collection.find().sort(ascending("age")).limit(1).iterator();
        System.out.println("Имя самого молодого = " + theYoungestCursor.next().getString("name"));
        MongoCursor<Document> theOldestCursor = collection.find().sort(descending("age")).limit(1).cursor();
        List<String> courses = theOldestCursor.next().get("courses", ArrayList.class);
        System.out.println("Список курсов самого старого :");
        courses.forEach(System.out::println);
        System.out.println("Aggregation");
        AggregateIterable<Document> aggregateIterable =
                collection.aggregate(Arrays.asList(
                        Aggregates.group("$age", Accumulators.sum("count", 1))
                ));
        for (Document doc : aggregateIterable)
        {
            System.out.println(doc);
        }
    }

    private static void parseCSV() throws Exception  //{name, age, courses}
    {
        List<String> students = Files.readAllLines(path);
        for (String student : students)
        {
            Document studentDocument = new Document();
            String[] params = student.split(",", 3);
            List<String> courses = Arrays.asList(params[2].replaceAll("\"", "").split(","));
            studentDocument
                    .append("name", params[0])
                    .append("age", Integer.parseInt(params[1]))
                    .append("courses", courses);
            collection.insertOne(studentDocument);
        }
    }
}
