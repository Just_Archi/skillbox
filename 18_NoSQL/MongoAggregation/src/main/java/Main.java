import Commands.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Scanner;

public class Main
{
    private static CommandExecutor commandExecutor;

    public static void main(String[] args)
    {
        commandExecutor = new CommandExecutor();
        parseGoods();//Эти методы для теста агрегации
        parseShops();
        Scanner scanner = new Scanner(System.in);
        while(true)
        {
            Command com = null;
            System.out.println("Введите команду");
            String command = scanner.nextLine();
            if (command.equals("СТАТИСТИКА_ТОВАРОВ"))
            {
                com = new StatsCommand();
            }
            else
            {
                String[] subCommands = command.split("\\s", 2);
                if (subCommands[0].equals("ДОБАВИТЬ_МАГАЗИН"))
                {
                    com = new AddShopCommand(subCommands[1]);
                }
                else
                {
                    String[] params = subCommands[1].split("\\s", 2);
                    if (subCommands[0].equals("ДОБАВИТЬ_ТОВАР"))
                    {
                        com = new AddGoodCommand(params[0], Integer.parseInt(params[1]));
                    }
                    if (subCommands[0].equals("ВЫСТАВИТЬ_ТОВАР"))
                    {
                        com = new PostGoodCommand(params[0], params[1]);
                    }
                }
            }
            commandExecutor.critStat();
            commandExecutor.execute(com);
        }
    }

    public static void parseShops()
    {
        try
    {
        List<String> shops = Files.readAllLines(Path.of("src/main/resources/shops.csv"));
        for (String shop : shops)
        {
            String[] params = shop.split(",", 2);
            commandExecutor.execute(new AddShopCommand(params[0]));
            for (String good : params[1].replaceAll("\"", "").split(","))
            {
                commandExecutor.execute(new PostGoodCommand(params[0], good));
            }
        }
    }
    catch (Exception e){e.printStackTrace();}
    }

    public static void parseGoods()
    {
        try
        {
            List<String> goods = Files.readAllLines(Path.of("src/main/resources/goods.csv"));
            for (String good : goods)
            {
                String[] params = good.split(",");
                commandExecutor.execute(new AddGoodCommand(params[0], Integer.parseInt(params[1])));
            }
        }
        catch (Exception e){e.printStackTrace();}
    }
}
