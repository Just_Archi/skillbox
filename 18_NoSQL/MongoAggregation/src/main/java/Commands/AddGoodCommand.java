package Commands;

import com.mongodb.client.MongoCollection;
import org.bson.Document;

public class AddGoodCommand implements Command
{
    private String goodName;
    private int price;

    public AddGoodCommand(String goodName, int price)
    {
        this.goodName = goodName;
        this.price = price;
    }
    @Override
    public void execute(MongoCollection<Document> shops, MongoCollection<Document> goods)
    {
        Document good = new Document();
        good.append("name", goodName);
        good.append("price", price);
        goods.insertOne(good);
    }
}
