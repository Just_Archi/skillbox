package Commands;

import com.mongodb.client.MongoCollection;

import org.bson.Document;

public interface Command
{
    void execute(MongoCollection<Document> shops, MongoCollection<Document> goods);
}
