package Commands;

import com.mongodb.client.MongoCollection;
import org.bson.Document;
import org.bson.conversions.Bson;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Updates.addToSet;

public class PostGoodCommand implements Command
{
    private String shopName;
    private String goodName;

    public PostGoodCommand(String shopName, String goodName)
    {
        this.shopName = shopName;
        this.goodName = goodName;
    }
    @Override
    public void execute(MongoCollection<Document> shops, MongoCollection<Document> goods)
    {
        Bson search = eq("name", shopName);
        Bson update = addToSet("goods", goodName);
        shops.findOneAndUpdate(search, update);
    }
}
