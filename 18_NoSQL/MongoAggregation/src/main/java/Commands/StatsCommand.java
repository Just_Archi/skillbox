package Commands;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Accumulators;
import com.mongodb.client.model.Field;
import org.bson.Document;
import org.bson.conversions.Bson;

import java.util.Arrays;

import static com.mongodb.client.model.Aggregates.*;

public class StatsCommand implements  Command
{
    public StatsCommand(){}
    @Override
    public void execute(MongoCollection<Document> shops, MongoCollection<Document> goods)
    {
        Bson addFields = addFields(new Field("n",
                new Document("$cond",
                        new Document("if", new Document("$gt", Arrays.asList("$good.price", 100L)))
                                .append("then", 0L)
                                .append("else", 1L))));
        shops.aggregate(Arrays.asList(
                unwind("$goods"),
                lookup("goods", "goods", "name", "good"),
                unwind("$good"),
                addFields,
                group("$name",
                        Accumulators.sum("Кол-во товаров в магазине", 1),
                        Accumulators.avg("Средняя цена товаров", "$good.price"),
                        Accumulators.max("Самый дорогой товар", "$good.price"),
                        Accumulators.min("Самый дешёвый товар", "$good.price"),
                        Accumulators.sum("Дешевле 100", "$n")//Как это сделать, я тоже не знаю
                )
        )).iterator().forEachRemaining(System.out::println);
    }
}
