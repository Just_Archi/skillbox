package Commands;

import com.mongodb.client.MongoCollection;
import org.bson.Document;

import java.util.ArrayList;

public class AddShopCommand implements Command
{
    public String shopName;
    public AddShopCommand(String shopName)
    {
        this.shopName = shopName;
    }

    @Override
    public void execute(MongoCollection<Document> shops, MongoCollection<Document> goods)
    {
        Document shop = new Document();
        shop.append("name", shopName);
        shop.append("goods", new ArrayList<>());
        shops.insertOne(shop);
    }
}
