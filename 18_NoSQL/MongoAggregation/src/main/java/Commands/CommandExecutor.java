package Commands;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

public class CommandExecutor
{
    private static MongoCollection<Document> shops;
    private static MongoCollection<Document> goods;
    public CommandExecutor()
    {
        MongoClient client = new MongoClient("localhost", 27017);
        MongoDatabase database = client.getDatabase("local");
        shops = database.getCollection("shops");
        goods = database.getCollection("goods");
        shops.drop();
        goods.drop();
    }

    public void execute(Command command)
    {
        if (command != null)
        {
            command.execute(shops, goods);
        }
    }

    public void critStat()
    {
        MongoCursor<Document> sh = shops.find().iterator();
        sh.forEachRemaining(System.out::println);
        MongoCursor<Document> gd = goods.find().iterator();
        gd.forEachRemaining(System.out::println);
    }
}
