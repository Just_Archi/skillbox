import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.*;

public class Main
{
    private static String defaultURL = "http://localhost:8000/";
    private static HttpURLConnection connection;
    private static Scanner scanner;
    private static ObjectMapper objectMapper = new ObjectMapper();

    public static void main(String[] args)
    {
        try
        {
            scanner = new Scanner(System.in);
            System.out.println("Введите URL (или слово default для подключения к http://localhost:8000/");
            String set = scanner.nextLine();
            if (!set.equals("default"))
            {
                defaultURL = set;
            }
            while (true)
            {
                System.out.println("Введите команду (или слово commands для получения списка команд)");
                String command = scanner.nextLine();
                switch (command)
                {
                    case "stop":
                        connection.disconnect();
                        break;
                    case "commands":
                        System.out.println("stop - остановка программы,\n getAll - для получения полного списка дел,\n get - для получения конкретного дела,\n add - для добавления дела,\n edit - для изменения дела,\n delete - для удаления дела");
                        break;
                    case "getAll":
                        getAll();
                        break;
                    case "get":
                        get();
                        break;
                    case "add":
                        add();
                        break;
                    case "edit":
                        edit();
                        break;
                    case "delete":
                        delete();
                        break;
                }
                if (command.equals("stop"))
                {
                    break;
                }
            }
        } catch (Exception ex) {ex.printStackTrace();}
    }

    private static void getAll() throws Exception
    {
        connection = (HttpURLConnection) new URL(defaultURL+"tasks").openConnection();
        connection.setRequestMethod("GET");
        BufferedReader in = new BufferedReader(
                new InputStreamReader(connection.getInputStream()));
        String inputLine;
        StringBuilder content = new StringBuilder();
        while ((inputLine = in.readLine()) != null) {
            content.append(inputLine);
        }
        in.close();
        List<Task> response = objectMapper.readValue(content.toString(), new TypeReference<List<Task>>(){});
        for (Task task : response)
        {
            System.out.println(task.toString());
        }
    }

    private static void get() throws Exception
    {
        System.out.println("Введите id нужного дела");
        int id = Integer.parseInt(scanner.nextLine());
        connection = (HttpURLConnection) new URL(defaultURL+id).openConnection();
        connection.setRequestMethod("GET");
        BufferedReader in = new BufferedReader(
                new InputStreamReader(connection.getInputStream()));
        String inputLine;
        StringBuilder content = new StringBuilder();
        while ((inputLine = in.readLine()) != null) {
            content.append(inputLine);
        }
        in.close();
        Task task = objectMapper.readValue(content.toString(), Task.class);
        System.out.println(task.toString());
    }

    private static void add() throws Exception
    {
        connection = (HttpURLConnection) new URL(defaultURL).openConnection();
        connection.setRequestMethod("POST");
        connection.setDoOutput(true);
        System.out.println("Введите текст нового дела");
        String info = scanner.nextLine();
        Map<String, String> params = new HashMap<>();
        params.put("info", info);
        StringJoiner sj = new StringJoiner("&");
        for(Map.Entry<String,String> entry : params.entrySet())
            sj.add(URLEncoder.encode(entry.getKey(), "UTF-8") + "="
                    + URLEncoder.encode(entry.getValue(), "UTF-8"));
        byte[] out = sj.toString().getBytes(StandardCharsets.UTF_8);
        int length = out.length;
        connection.setFixedLengthStreamingMode(length);
        connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        connection.connect();
        try(OutputStream os = connection.getOutputStream()) {
            os.write(out);
            os.flush();
        }

    }

    private static void edit() throws Exception
    {
        getAll();
        System.out.println("Выберите, какое дело хотите изменить");
        String id = scanner.nextLine();
        connection = (HttpURLConnection) new URL(defaultURL+id).openConnection();
        connection.setRequestMethod("POST");
        connection.setDoOutput(true);
        System.out.println("Введите изменённый текст дела");
        String info = scanner.nextLine();
        Map<String, String> params = new HashMap<>();
        params.put("info", info);
        StringJoiner sj = new StringJoiner("&");
        for(Map.Entry<String,String> entry : params.entrySet())
            sj.add(URLEncoder.encode(entry.getKey(), "UTF-8") + "="
                    + URLEncoder.encode(entry.getValue(), "UTF-8"));
        byte[] out = sj.toString().getBytes(StandardCharsets.UTF_8);
        int length = out.length;
        connection.setFixedLengthStreamingMode(length);
        connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        connection.connect();
        try(OutputStream os = connection.getOutputStream()) {
            os.write(out);
            os.flush();
        }
    }

    private static void delete() throws Exception
    {
        getAll();
        System.out.println("Выберите, какое дело хотите удалить");
        String id = scanner.nextLine();
        connection = (HttpURLConnection) new URL(defaultURL+id).openConnection();
        connection.setRequestMethod("DELETE");
        connection.setDoOutput(true);
        connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        connection.connect();
        connection.getResponseCode();
    }
}
