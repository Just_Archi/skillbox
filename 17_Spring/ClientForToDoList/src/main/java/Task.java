import java.util.Objects;

public class Task
{
    private int id;
    private String info;
    public Task(){}

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getInfo()
    {
        return info;
    }

    public void setInfo(String info)
    {
        this.info = info;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Task task = (Task) o;
        return id == task.id && Objects.equals(info, task.info);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(id, info);
    }

    @Override
    public String toString()
    {
        return id + " - " + info;
    }
}
