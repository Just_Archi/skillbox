package main.controllers;

import main.task.Task;
import main.task.TaskService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
public class TaskController
{
    private TaskService taskService;

    public TaskController(TaskService taskService)
    {
        this.taskService = taskService;
    }

    @RequestMapping("/tasks")
    public List<Task> list()
    {
        return taskService.getAllTasks();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Task> get(@PathVariable int id)
    {
        return taskService.get(id);
    }

    @PostMapping("/")
    public Integer add(@RequestParam String info)
    {
        return taskService.add(info);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Boolean> delete(@PathVariable int id)
    {
        return taskService.delete(id);
    }

    @PostMapping("/{id}")
    public ResponseEntity<String> edit(@PathVariable int id, @RequestParam String info)
    {
        return taskService.edit(id, info);
    }
}
