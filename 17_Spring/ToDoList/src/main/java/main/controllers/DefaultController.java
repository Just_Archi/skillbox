package main.controllers;

import main.task.Task;
import main.task.TaskService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class DefaultController
{
    private TaskService taskService;

    public DefaultController(TaskService taskService)
    {
        this.taskService = taskService;
    }

    @GetMapping("/")
    public String index(Model model)
    {
        List<Task> tasks = taskService.getAllTasks();
        model.addAttribute("tasks", tasks);
        return "index";
    }
}
