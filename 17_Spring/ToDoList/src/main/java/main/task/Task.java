package main.task;

import javax.persistence.*;

@Entity
@Table(name = "Tasks")
public class Task
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String info;

    public Task(String info) //Делал не через мапу и строки, а через класс, потому что потом понадобится прописать маппинг с БД
    {
        this.info = info;
    }

    public Task(){}

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getInfo()
    {
        return info;
    }

    public void setInfo(String info)
    {
        this.info = info;
    }

    @Override
    public String toString()
    {
        return id + " - " + info;
    }
}
