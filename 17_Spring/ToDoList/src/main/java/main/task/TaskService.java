package main.task;

import main.exceptions.EntityNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TaskService
{

    private TaskRepository taskRepository;

    public TaskService(TaskRepository taskRepository) {this.taskRepository = taskRepository;}

    public List<Task> getAllTasks()
    {
        ArrayList<Task> tasks = new ArrayList<>();
        for (Task task : taskRepository.findAll())
        {
            tasks.add(task);
        }
        return tasks;
    }

    public ResponseEntity<Task> get(int id)
    {
        Task task = taskRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Task entity with id " + id + " not found"));
        return new ResponseEntity<>(task, HttpStatus.OK);
    }

    public Integer add(String info)
    {
        return taskRepository.save(new Task(info)).getId();
    }

    public ResponseEntity<Boolean> delete(int id)
    {
        if (!taskRepository.existsById(id))
        {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
        taskRepository.deleteById(id);
        return new ResponseEntity<Boolean>(true, HttpStatus.OK);
    }

    public ResponseEntity<String> edit(int id, String info)
    {
        Task task = taskRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Task entity with id " + id + " not found"));
        task.setInfo(info);
        taskRepository.save(task);
        return new ResponseEntity<String>(info, HttpStatus.OK);
    }
}
