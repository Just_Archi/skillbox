package main.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler
{
    @ExceptionHandler(EntityNotFoundException.class)
    public final ResponseEntity<String> handleExceptions(Exception ex)
    {
        if (ex instanceof EntityNotFoundException)
        {
            return entityNotFoundExceptionHandle(ex.getMessage());
        } else
        {
            return defaultExceptionHandle();
        }
    }


    private ResponseEntity<String> entityNotFoundExceptionHandle(String message)
    {
        return new ResponseEntity<>(message, HttpStatus.NOT_FOUND);
    }

    private ResponseEntity<String> defaultExceptionHandle()
    {
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
