import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class Bank
{

    private Map<String, Account> accounts = new HashMap<>();
    private final Random random = new Random();

    public Bank(){}

    public void addAccounts(Account... accounts)
    {
        for (Account account : accounts) //А как тут без цикла for?
        {
            this.accounts.put(account.getAccNumber(), account);
        }
    }

    public synchronized boolean isFraud(String fromAccountNum, String toAccountNum, long amount)//А, собственно, зачем тут amount?
        throws InterruptedException
    {
        Thread.sleep(1000);
        return random.nextBoolean();
    }

    /**
     * TODO: реализовать метод. Метод переводит деньги между счетами. Если сумма транзакции > 50000,
     * то после совершения транзакции, она отправляется на проверку Службе Безопасности – вызывается
     * метод isFraud. Если возвращается true, то делается блокировка счетов (как – на ваше
     * усмотрение)
     */
    public void transfer(String fromAccountNum, String toAccountNum, long amount)
    throws InterruptedException
    {
        Account from = accounts.get(fromAccountNum);
        Account to = accounts.get(toAccountNum);
        if (checkTransferPossibility(from, to, amount))
        {
            synchronized (from)
            {
                from.decreaseMoney(amount);
                to.increaseMoney(amount);
            }
            if (amount > 50000)
            {
                if (isFraud(fromAccountNum, toAccountNum, amount))
                {
                    System.out.println("ILLEGAL!!!!!");
                    from.markUnlegal();
                    to.markUnlegal();
                }
            }
        }
    }

    public boolean checkTransferPossibility(Account from, Account to, long amount)
    {
        return from.getMoney() >= amount && from.getLegality() && to.getLegality()
                && !from.equals(to);
    }

    /**
     * TODO: реализовать метод. Возвращает остаток на счёте.
     */
    public long getBalance(String accountNum)
    {
        return accounts.get(accountNum).getMoney();
    }

    public long getSumAllAccounts()
    {
        long sum = 0;
        for (Account account : accounts.values())
        {
            sum += account.getMoney();
        }
        return sum;
    }
}

/**
 *Маленький вопрос: можно ли настроить Idea так, чтобы она форматировала фигурные скобки не так:
 * public method(){
 *     some_code;
 * }
 *   а так:
 *   public method()
 *   {
 *       some_code;
 *   }
 */
