import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;

public class Account {

    private AtomicLong money = new AtomicLong();
    private String accNumber;
    private boolean legality;

    public Account(long money, String accNumber) {
        this.money.set(money);
        this.accNumber = accNumber;
        legality = true;
    }

    public long getMoney() {
        return money.get();
    }

    public void setMoney(AtomicLong money) {
        this.money = money;
    }

    public String getAccNumber() {
        return accNumber;
    }

    public void setAccNumber(String accNumber) {
        this.accNumber = accNumber;
    }

    public boolean getLegality() {  return legality; }

    public void markUnlegal() { this.legality = false; }

    public void increaseMoney(long amount) {money.addAndGet(amount);}

    public void decreaseMoney(long amount) {money.addAndGet(-amount);}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return legality == account.legality && Objects.equals(money, account.money) && Objects.equals(accNumber, account.accNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(money, accNumber, legality);
    }

    @Override
    public String toString()
    {
        return accNumber + " - " + money;
    }
}
