import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class BankTest extends TestCase
{
    private Bank bank;
    List<Transfer> transferList;

    @Before
    public void setUp()
    {
        bank = new Bank();
        List<Account> accounts = generateAccounts(10);
        accounts.forEach(account -> System.out.println(account.toString()));
        accounts.forEach(bank::addAccounts);
        transferList = generateTransfers(accounts);
        transferList.forEach(transfer -> System.out.println(transfer.toString()));
    }

    @Test
    public void testTransfers() throws ExecutionException, InterruptedException
    {
        long beginSum = bank.getSumAllAccounts();

        ExecutorService executorService = Executors.newFixedThreadPool(10);
        List<Future<?>> tasks = new ArrayList<>();

        for (Transfer transfer : transferList)
        {
        tasks.add(executorService.submit(transfer));
        }

        for (Future<?> task: tasks) {
            task.get();
        }

        assertEquals(beginSum, bank.getSumAllAccounts());

//        long beginSum = bank.getSumAllAccounts();
//        Executor executor = (runnable) -> new Thread(runnable).start();
//        transferList.forEach(executor::execute);
//        assertEquals(beginSum, bank.getSumAllAccounts());
    }


    private class Transfer implements Runnable
    {
        private Account from;
        private Account to;
        public Transfer(Account from, Account to)
        {
            this.from = from;
            this.to = to;
        }
        @Override
        public void run()
        {
            for (int i = 0; i < 1000; i++)
            {
                try
                {
                    bank.transfer(from.getAccNumber(), to.getAccNumber(), (long) (Math.random()*280000 + 20000));
                    System.out.println("success: from " + from.getAccNumber() + " to " + to.getAccNumber());
                } catch (InterruptedException e)
                {
                    System.out.println("fail: from " + from.getAccNumber() + " to " + to.getAccNumber());
                    e.printStackTrace();
                }
            }
        }

        @Override
        public String toString()
        {
            return "From: " + from.toString() + "; to : " + to.toString();
        }
    }

    private List<Account> generateAccounts(int count)
    {
        return new ArrayList<>()
        {{
            for (int i = 0; i < count; i++)
            {
                add(new Account((long) (Math.random()*500000 + 500000), Character.toString('a' + i)));
            }
        }};
    }

    private List<Transfer> generateTransfers(List<Account> accounts)
    {
        return new ArrayList<>()
        {{
                for (int i = 0; i < accounts.size(); i++)
                {
                    if (i == (accounts.size() - 1))
                    {
                        add(new Transfer(accounts.get(i), accounts.get(0)));
                    }
                    else
                    {
                        add(new Transfer(accounts.get(i), accounts.get(i + 1)));
                    }
                }
        }};
    }
}
