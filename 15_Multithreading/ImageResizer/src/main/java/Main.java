import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        String srcFolder = "";
        String dstFolder = "D:\\Skillbox\\forskillbox\\15_Multithreading\\ImageResizer\\src\\main\\resources";
//        new Thread(new Resizer(new File(srcFolder).listFiles(), dstFolder)).start();

        multithreadingResize(srcFolder, dstFolder, 4);
    }

    public static void multithreadingResize(String srcFolder, String dstFolder, int coresCount)
    /*
     *Существует метод Runtime.getRuntime().getAvailableProcessors(), но он возврщает не количество ядер,
     * а количество логических потоков, а это нам не надо
     */
    {
        File srcDir = new File(srcFolder);
        File[] files = srcDir.listFiles();
        if (files == null) {return;}
        List<File[]> filesByCores = new ArrayList<>()
        {{
            for (int i = 0; i < coresCount; i++)
            {
                add(new File[Math.round(files.length / coresCount) + 1]);
            }
        }};

        int currentElementIndex = 0;
        int currentArrayIndex = 0;

        for (File file : files)
        {
            if (currentArrayIndex == filesByCores.size())
            {
                currentArrayIndex = 0;
                currentElementIndex++;
            }
            filesByCores.get(currentArrayIndex)[currentElementIndex] = file;
            currentArrayIndex++;
        }

        filesByCores.forEach(arr ->
        {
            Resizer resizer = new Resizer(arr, dstFolder);
            Thread thread = new Thread(resizer);
            thread.start();
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
    }
}
