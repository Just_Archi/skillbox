import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.RecursiveTask;

public class ReferencesCollector extends RecursiveTask<List<String>>
{
    public static String absoluteRef = null;
    public String currentRef;
    int tabulationsCount;
    static Set<String> addedLinks;

    public ReferencesCollector(String absoluteRef)
    {
        this.absoluteRef = absoluteRef;
        currentRef = null;
        tabulationsCount = 0;
        addedLinks = ConcurrentHashMap.newKeySet();
    }

    private ReferencesCollector(String currentRef, int tabulationsCount)
    {
        this.currentRef = currentRef;
        this.tabulationsCount = tabulationsCount;
    }

    @Override
    protected List<String> compute()
    {
        List<String> references = new ArrayList<>();
        List<ReferencesCollector> taskList = new ArrayList<>();
        if (currentRef == null)
        {
            references.add(absoluteRef);
            currentRef = absoluteRef;
        }
        else
        {
            references.add(currentRef);
        }
        try
        {
            Thread.sleep(150);
            Document document = Jsoup.connect(currentRef).get();
            Elements elements = document.select("a");
            for (Element element : elements)
            {
                String taskRef = refEditor(element.attr("href"), tabulationsCount);
                if (taskRef != null)
                {
                    addedLinks.add(taskRef);

                    ReferencesCollector task = new ReferencesCollector(taskRef, ++tabulationsCount);
                    task.fork();
                    taskList.add(task);
                    //taskList.add(task.fork()); выдаёт ошибку RequiredType Provided
                }
            }
        } catch (Exception exception){exception.printStackTrace();}

        for(ReferencesCollector task : taskList)
        {
            references.addAll(task.join());
        }
        return references;
    }

    private String refEditor(String rawRef, int tabulationsCount)
    {
        System.out.println(rawRef);
        if (rawRef == currentRef|| rawRef == absoluteRef || addedLinks.contains(rawRef)){
            return null;
        }
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < tabulationsCount; i++)
        {
            stringBuffer.append("\t");
        }
        if (rawRef.contains(absoluteRef))
        {
            stringBuffer.append(rawRef);
        }
        else if (rawRef.matches("/[0-9A-Za-z-_]+/"))
        {
            stringBuffer.append(absoluteRef).append(rawRef);
        }
        else{return null;}
        return stringBuffer.toString();
    }
}
