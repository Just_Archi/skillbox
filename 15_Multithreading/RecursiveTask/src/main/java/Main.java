import java.util.List;
import java.util.concurrent.ForkJoinPool;

public class Main
{
    public static String absoluteRef = "https://skillbox.ru";
    public static void main(String[] args)
    {
        List<String> list = new ForkJoinPool().invoke(new ReferencesCollector(absoluteRef));
        list.forEach(System.out::println);

    }
}
