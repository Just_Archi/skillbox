import javax.swing.*;
import java.awt.*;

public class Main
{
    private static CollapsePanel collapsePanel = new CollapsePanel();
    private static ExpandPanel expandPanel = new ExpandPanel();
    public static void main(String[] args)
    {
        JFrame frame = new JFrame();
        frame.setSize(600, 400);
        frame.setLayout(new FlowLayout());
        frame.setLocationRelativeTo(null);
        frame.add(collapsePanel.getMainPanel());
        frame.add(expandPanel.getMainPanel());
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }

    public static void displayCollapsePanel()
    {
        collapsePanel.setVisibility(true);
        expandPanel.setVisibility(false);
    }

    public static void displayExpandPanel()
    {
        collapsePanel.setVisibility(false);
        expandPanel.setVisibility(true);
    }

    public static void setExpandPanelText(String text)
    {
        expandPanel.setFullNameText(text);
    }

    public static void setCollapsePanelText(String[] strings)
    {
        collapsePanel.setFields(strings);
    }
}
