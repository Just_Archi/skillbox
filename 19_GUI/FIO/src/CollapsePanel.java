import javax.swing.*;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeListener;

public class CollapsePanel
{
    private JPanel mainPanel;
    private JLabel SurnameLabel;
    private JTextField SurnameText;
    private JLabel NameLabel;
    private JTextField NameText;
    private JLabel FathernameLabel;
    private JTextField FathernameText;
    private JButton CollapseButton;

    public JPanel getMainPanel()
    {
        return mainPanel;
    }

    public void setVisibility(boolean visibility)
    {
        mainPanel.setVisible(visibility);
    }

    public CollapsePanel()
    {
        CollapseButton.addActionListener(new Action()
        {
            @Override
            public Object getValue(String key)
            {
                return null;
            }

            @Override
            public void putValue(String key, Object value)
            {

            }

            @Override
            public void setEnabled(boolean b)
            {

            }

            @Override
            public boolean isEnabled()
            {
                return false;
            }

            @Override
            public void addPropertyChangeListener(PropertyChangeListener listener)
            {

            }

            @Override
            public void removePropertyChangeListener(PropertyChangeListener listener)
            {

            }

            @Override
            public void actionPerformed(ActionEvent e)
            {
                String surname = SurnameText.getText();
                String name = NameText.getText();
                String fatherName = FathernameText.getText();
                if (surname.length() == 0 || name.length() == 0)
                {
                    JOptionPane.showMessageDialog(mainPanel, "Empty name or surname", "Incorrect input", JOptionPane.ERROR_MESSAGE);
                }
                else
                {
                    StringBuilder builder = new StringBuilder();
                    builder.append(surname);
                    builder.append(" ");
                    builder.append(name);
                    if (fatherName.length() != 0)
                    {
                    builder.append(" ");
                    builder.append(fatherName);
                    }
                    Main.setExpandPanelText(builder.toString());
                    Main.displayExpandPanel();
                }
            }
        });
    }

    public void setFields(String[] strings)
    {
        SurnameText.setText(strings[0]);
        NameText.setText(strings[1]);
        if (strings.length == 3)
        {
            FathernameText.setText(strings[2]);
        }
        else
        {
            FathernameText.setText("");
        }
    }
}
