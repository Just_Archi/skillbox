import javax.swing.*;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeListener;

public class ExpandPanel
{
    private JPanel mainPanel;
    private JLabel FullNameLabel;
    private JButton ExpandButton;
    private JTextField FullNameText;

    public JPanel getMainPanel()
    {
        return mainPanel;
    }

    public void setVisibility(boolean visibility)
    {
        mainPanel.setVisible(visibility);
    }

    public ExpandPanel()
    {
        ExpandButton.addActionListener(new Action()
        {
            @Override
            public Object getValue(String key)
            {
                return null;
            }

            @Override
            public void putValue(String key, Object value)
            {

            }

            @Override
            public void setEnabled(boolean b)
            {

            }

            @Override
            public boolean isEnabled()
            {
                return false;
            }

            @Override
            public void addPropertyChangeListener(PropertyChangeListener listener)
            {

            }

            @Override
            public void removePropertyChangeListener(PropertyChangeListener listener)
            {

            }

            @Override
            public void actionPerformed(ActionEvent e)
            {
                String text = FullNameText.getText().trim();
                String[] strings = text.split("\\s+", 3);
                if (strings.length > 1)
                {
                    Main.setCollapsePanelText(strings);
                    Main.displayCollapsePanel();
                }
                else
                {
                    JOptionPane.showMessageDialog(mainPanel, "Not enough words", "Incorrect input", JOptionPane.ERROR_MESSAGE);
                }

            }
        });
    }

    public void setFullNameText(String replacement)
    {
        FullNameText.setText(replacement);
    }
}
