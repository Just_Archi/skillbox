package main;

import net.ReferencesCollector;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;
import java.util.concurrent.ForkJoinPool;
@SpringBootApplication
public class Main
{
    public static String absoluteRef = "http://www.playback.ru/";


    public static void main(String[] args)
    {
        SpringApplication.run(Main.class, args);
//        absoluteRef = cutLastSlash(absoluteRef);
//        List<String> cutRefs = new ForkJoinPool().invoke(new ReferencesCollector(absoluteRef));
//
//        for (String cutRef : cutRefs)
//        {
//            System.out.println(cutRef);
//        }
    }

    public static String cutLastSlash(String str)
    {
        if (str.lastIndexOf('/') == (str.length() - 1))
        {
            str = str.substring(0, str.length() - 1);
        }
        return str;
    }
}
