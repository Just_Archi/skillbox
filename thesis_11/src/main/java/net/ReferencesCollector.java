package net;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.RecursiveTask;

public class ReferencesCollector extends RecursiveTask<List<String>>
{
    public String absoluteRef;
    public String currentRef;
    static Set<String> addedLinks;

    public ReferencesCollector(String absoluteRef)
    {

        this.absoluteRef = absoluteRef;
        currentRef = null;
        addedLinks = ConcurrentHashMap.newKeySet();
    }

    private ReferencesCollector(String currentRef, String absoluteRef)
    {
        this.currentRef = currentRef;
        this.absoluteRef = absoluteRef;
    }

    @Override
    protected List<String> compute()
    {
        List<String> references = new ArrayList<>();
        List<ReferencesCollector> taskList = new ArrayList<>();
        if (currentRef == null)
        {
            currentRef = "/";
            addedLinks.add(currentRef);
        }
        references.add(currentRef);
        try
        {
            Thread.sleep(5000);
            Document document;
            String ref = absoluteRef + currentRef;
            Connection.Response response = Jsoup.connect(ref)
                    .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                    .referrer("http://www.google.com").execute();
            document = response.parse();
            Elements elements = document.select("a");
            for (Element element : elements)
            {
                String taskRef = element.attr("href");
                if (!(addedLinks.contains(taskRef) || taskRef.contains("http:") || taskRef.contains("https:")))
                {
                    addedLinks.add(taskRef);
                    ReferencesCollector task = new ReferencesCollector(taskRef, absoluteRef);
                    task.fork();
                    taskList.add(task);
                }
            }
        } catch (Exception e)
        {
            e.printStackTrace();
        }

        for (ReferencesCollector task : taskList)
        {
            references.addAll(task.join());
        }
        return references;
    }
}

